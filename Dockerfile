FROM rust:slim-buster as build

# create a new empty shell project
RUN USER=root cargo new --bin pool_pump_controller
WORKDIR /pool_pump_controller

# copy over your manifests
COPY ./Cargo.lock ./Cargo.lock
COPY ./Cargo.toml ./Cargo.toml

RUN apt-get update -y
RUN apt-get install -y libssl-dev
RUN apt-get install -y pkg-config

# this build step will cache your dependencies
RUN cargo build --release
RUN rm src/*.rs

# copy your source tree
COPY ./src ./src

# build for release
RUN rm ./target/release/deps/pool_pump_controller*
RUN cargo build --release

# our final base
FROM rust:slim-buster

# copy the build artifact from the build stage
COPY --from=build /pool_pump_controller/target/release/pool_pump_controller .

# set the startup command to run your binary
CMD ["./pool_pump_controller"]