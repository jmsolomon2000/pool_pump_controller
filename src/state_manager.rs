use std::fs::File;
use std::io::Read;
use firestore::*;
use reqwest::{blocking, StatusCode};
use serde::{Serialize, Deserialize};

const STATE_COLLECTION_NAME: &str = "pool-pump-controller";
const STATE_DOCUMENT_ID: &str = "pool-pump-state";
const SCHEDULE_DOCUMENT_ID: &str = "pool-pump-schedule";

const SWITCH_URL: &str = "http://192.168.68.77";
const SWITCH_STATUS_PATH: &str = "/rpc/Switch.GetStatus?id=0";
const SWITCH_ON_PATH: &str = "/rpc/Switch.Set?id=0&on=true";
const SWITCH_OFF_PATH: &str = "/rpc/Switch.Set?id=0&on=false";
const SCHEDULE_CONFIG_FILE: &str = "pump_schedule.json";

#[derive(PartialEq, Eq, Clone, Copy, Deserialize, Serialize, Debug)]
pub enum PumpState {
    On,
    ForcedOn,
    Off
}

#[derive(Debug, Deserialize, Serialize)]
struct Temperature {
    tC: f64,
    tF: f64,
}

#[derive(Debug, Deserialize, Serialize)]
struct SwitchGetStatus {
    id: i32,
    source: String,
    output: bool,
    temperature: Temperature,
}

#[derive(Debug, Deserialize, Serialize)]
struct SwitchSetStatus {
    id: i32,
    on: bool,
}

#[derive(Debug, Clone, Deserialize, Serialize)]
struct PumpStateDocument {
    current_state: PumpState
}

#[derive(Debug, Clone, Deserialize, Serialize)]
pub struct PumpScheduleDocument {
    pub start_hour: u32,
    pub end_hour: u32,
    pub temp_threshold_c: f64
}

impl PumpScheduleDocument {
    pub fn default() -> PumpScheduleDocument {
        PumpScheduleDocument { start_hour: 4, end_hour: 12, temp_threshold_c: 3.0 }
    }
}

//#[tokio::main]
pub fn get_current_pump_state() -> Result<PumpState, Box<dyn std::error::Error + Send + Sync>> {
    let resp = blocking::get(format!("{}{}", SWITCH_URL, SWITCH_STATUS_PATH))?;
    let status: SwitchGetStatus = resp.json()?;
    let current_state = status.output;

    if current_state {
        Ok(PumpState::On)
    } else {
        Ok(PumpState::Off)
    }

}

//#[tokio::main]
pub fn set_current_pump_state(pump_state: PumpState) -> Result<(), Box<dyn std::error::Error + Send + Sync>> {
    let action_path = match pump_state {
        PumpState::On => SWITCH_ON_PATH,
        PumpState::ForcedOn => SWITCH_ON_PATH,
        PumpState::Off => SWITCH_OFF_PATH
    };

    let resp = blocking::get(format!("{}{}", SWITCH_URL, action_path));

    match resp {
        Ok(_) => {
            Ok(())
        }
        Err(err) => {
            Err(err.into())
        }
    }
}

pub fn get_pump_schedule() -> PumpScheduleDocument {
    println!("Retrieving pump schedule from filesystem...");
    let schedule_result = _get_pump_schedule(SCHEDULE_CONFIG_FILE);
    if let Ok(result) = schedule_result {
        result
    }  else {
        println!("ERROR: Unable to retrieve schedule from Firestore!! Falling back to defaults! {}", schedule_result.unwrap_err());
        PumpScheduleDocument::default()
    }
}

//#[tokio::main]
 fn _get_pump_schedule(pump_schedule_file: &str) -> Result<PumpScheduleDocument, Box<dyn std::error::Error + Send + Sync>> {
    let app_state = if let Ok(mut file) = File::open(pump_schedule_file) {
        let mut content = String::new();
        file.read_to_string(&mut content).expect(format!("Failed to read: {}", SCHEDULE_CONFIG_FILE).as_str());
        serde_json::from_str(&content).expect("Failed to deserialize JSON")
    } else {
        // If the file doesn't exist, create a new default state
        println!("ERROR: Unable to retrieve schedule from filesystem!! Falling back to defaults!");
        PumpScheduleDocument::default()
    };

    Ok(app_state)

}

mod tests {
    use std::fs::File;
    use std::io::Write;
    use serde::serde_if_integer128;
    use crate::state_manager::PumpScheduleDocument;

    #[test]
    fn test_pump_state_write() {
        let result =  super::set_current_pump_state(super::PumpState::On);
        if result.is_err() {
            println!("{}", result.err().unwrap());
            panic!("Test fail!")
        } else {
            assert!(result.is_ok())
        }
    }

    #[test]
     fn test_pump_state_read() {
        let result =  super::get_current_pump_state();
        assert_eq!(super::PumpState::Off, result.unwrap());
    }

    #[test]
     fn test_pump_schedule_default() {
        let result =  super::_get_pump_schedule("something_that_doesnt_exist.json");
        assert!(result.is_ok());
        let schedule = result.unwrap();
        assert_eq!(schedule.start_hour, 4);
        assert_eq!(schedule.end_hour, 12);
        assert_eq!(schedule.temp_threshold_c, 3.0);
    }

    #[test]
    fn test_pump_schedule_read_file() {
        let schedule = PumpScheduleDocument {
            start_hour: 12,
            end_hour: 21,
            temp_threshold_c: 2.0,
        };

        let mut file = File::create("pump_schedule_test.json").unwrap();
        file.write_all(serde_json::to_string(&schedule).unwrap().as_bytes()).expect("File to write test file");
        let result =  super::_get_pump_schedule("pump_schedule_test.json");

        assert!(result.is_ok());
        let schedule = result.unwrap();
        assert_eq!(schedule.start_hour, 12);
        assert_eq!(schedule.end_hour, 21);
        assert_eq!(schedule.temp_threshold_c, 2.0);
    }
}