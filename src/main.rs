mod weather;
mod state_manager;

use chrono::prelude::*;
use reqwest::blocking::Response;
use reqwest::StatusCode;
use state_manager::*;
use weather::aviationweather::AviationWeatherFetcher;

use crate::weather::weatherfetcher::WeatherFetcher;

const TURNPUMPONURL: &str  = "https://api.voicemonkey.io/trigger?access_token=f7bdc2a7097d4e0e5afc54b1e41fa054&secret_token=1089b5ad85e1c4625e1f52be60667609&monkey=turn-on-pool-pump&announcement=Pump%20Controller%20Turning%20On%20Pool%20Pump";
const TURNPUMPOFFURL: &str  = "https://api.voicemonkey.io/trigger?access_token=f7bdc2a7097d4e0e5afc54b1e41fa054&secret_token=1089b5ad85e1c4625e1f52be60667609&monkey=turn-off-pool-pump&announcement=Pump%20Controller%20Turning%20Off%20Pool%20Pump";

fn main() {
    println!("Pool pump controller started");
    println!("Determining what the pool pump status should be");

    let schedule = get_pump_schedule();
    
    println!("Evaluating weather...");
    let fetcher = AviationWeatherFetcher::new();
    let current_temp = fetcher.get_current_temperature();//get_current_weather(WEATHERURL);
    println!("Current outside temperature is: {current_temp}c");

    if current_temp < schedule.temp_threshold_c {
        println!("Temperature below minimum threshold ({}c)...forcing pump on every interval", schedule.temp_threshold_c);
        set_pool_pump_state(PumpState::ForcedOn);
    } else {
        println!("Temperature is above threshold ({}c)...evaluating schedule", schedule.temp_threshold_c);

        let dt_now = Utc::now();
        let current_hour = dt_now.hour();
        
        println!("Schedule: Start: {}:00-UTC End: {}:00-UTC", schedule.start_hour, schedule.end_hour);
        evaluate_schedule(current_hour, schedule.start_hour, schedule.end_hour, || set_pool_pump_state(PumpState::On), || set_pool_pump_state(PumpState::Off));
        
    }

    
}

fn evaluate_schedule(current_hr: u32, start_hr: u32, end_hr: u32, within_schedule_action: fn(), outside_schedule_action: fn()) {
    if start_hr > end_hr { // pump is on overnight
        if current_hr >= start_hr || current_hr < end_hr {
            within_schedule_action();
        } else {
            outside_schedule_action();
        }
    } else if current_hr >= start_hr && current_hr < end_hr {
            within_schedule_action();
        } else {
            outside_schedule_action();
        }
}

fn set_pool_pump_state(desired_state : PumpState) {

    let current_state = get_current_pump_state().unwrap();
    
    if current_state == desired_state && current_state != PumpState::ForcedOn {
        println!("Current state ({current_state:?}) matches previous state, so not sending command.")
    } else {
        let req: reqwest::Result<Response> = match desired_state {
            PumpState::On => {
                println!("Turning on pool pump");
                reqwest::blocking::get(TURNPUMPONURL)
            },
            PumpState::ForcedOn => {
                println!("Turning on pool pump (Forced)");
                reqwest::blocking::get(TURNPUMPONURL)
            }
            PumpState::Off => {
                println!("Turning off pool pump");
                reqwest::blocking::get(TURNPUMPOFFURL)
            }
        };

        match req {
            Ok(resp) => {
                if resp.status().is_success() {
                    println!("Successfully sent message to voicemonkey.io");
                    let result = set_current_pump_state(desired_state);
                    if result.is_err() {
                        println!("ERROR: Failed to store pump state in Firestore!! {}", result.unwrap_err())
                    }
                } else {
                    println!("ERROR: Failed to send message to voicemonkey.io. StatusCode: {}", resp.status())
                }
            }
            Err(error) => {
                println!("ERROR: Failed to send message to voicemonkey.io. StatusCode: {}", error.status().unwrap_or(StatusCode::INTERNAL_SERVER_ERROR))
            }
        }
    }
}

/*
https://api.voicemonkey.io/trigger?access_token=f7bdc2a7097d4e0e5afc54b1e41fa054&secret_token=1089b5ad85e1c4625e1f52be60667609&monkey=turn-on-pool-pump&announcement=Pool%20pump%20being%20turned%20on%20due%20to%20low%20outside%20temperature
 */