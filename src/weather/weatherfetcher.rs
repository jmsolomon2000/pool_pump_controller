pub trait WeatherFetcher {
    fn get_current_temperature(self) -> f64;
}