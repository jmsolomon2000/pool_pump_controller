use std::io::BufReader;

use xml::{EventReader, reader::XmlEvent};

use crate::weather::weatherfetcher::WeatherFetcher;
use reqwest::StatusCode;

const WEATHERURL: &str = "https://aviationweather.gov/cgi-bin/data/metar.php?ids=KPUJ&hours=0&order=id%2C-obs&sep=true&format=xml";
const BACKUP_WEATHERURL: &str = "https://aviationweather.gov/cgi-bin/data/metar.php?ids=KVPC&hours=0&order=id%2C-obs&sep=true&format=xml";

pub struct AviationWeatherFetcher {
    primary_url: String,
    backup_url: String
}

impl WeatherFetcher for AviationWeatherFetcher {
    fn get_current_temperature(self) -> f64 {
        self.internal_get_current_temperature(&self.primary_url)
    }
}

impl AviationWeatherFetcher {
    pub fn new() -> AviationWeatherFetcher {
        AviationWeatherFetcher { primary_url: WEATHERURL.to_string(), backup_url: BACKUP_WEATHERURL.to_string()}
    }

    #[cfg(test)]
    pub fn with_urls(primary_url: &str, backup_url: &str) -> AviationWeatherFetcher {
        AviationWeatherFetcher { primary_url: primary_url.to_owned(), backup_url: backup_url.to_owned()}
    }
}

impl AviationWeatherFetcher {
    fn internal_get_current_temperature(&self, weather_url: &String) -> f64 {
        println!("Getting weather from internet");
        let req = reqwest::blocking::get(weather_url);
        let mut current_temp_c = f64::MAX;
    
        match req {
            Ok(resp) => {
                let data = BufReader::new(resp);
                let parser = EventReader::new(data);
    
                let mut on_temp_tag = false;
    
    
                println!("evaluating weather data from internet");
                for e in parser {
    
                    match e {
                        Ok(XmlEvent::StartElement {name, ..}) => {
                            on_temp_tag = name.local_name == "temp_c";
                        },
                        Ok(XmlEvent::Characters(chars)) => {
                            if on_temp_tag {
                                current_temp_c = chars.parse().unwrap();
                                break;
                            }
                        }
                        Err(e) => {
                            println!("Unable to retrieve weather from XML: {e}");
                            break;
                        },
                        _ => {}
                    }
                }
            }
            Err(error) => {
                if self.backup_url.ne(weather_url) {
                    println!("Retrieving weather data from backup source");
                    return self.internal_get_current_temperature(&self.backup_url)
                } else {
                    println!("ERROR: Unable to retrieve internet weather data. StatusCode: {}", error.status().unwrap_or(StatusCode::INTERNAL_SERVER_ERROR));
                }
            }
        }
    
        if current_temp_c == f64::MAX {
            println!("Missing temperature data reported from data source");
    
            if self.backup_url.ne(weather_url) {
                println!("Retrieving weather data from backup source");
                return self.internal_get_current_temperature(&self.backup_url)
            } else {
                println!("ERROR: Missing temperature data from both primary and backup sources. Temp feature not working!");
            }
        }
    
        current_temp_c
    }
}
#[cfg(test)]
mod aviationweather_tests {
    
    use mockito::mock;
    use crate::weather::weatherfetcher::WeatherFetcher;
    

    #[test]
    pub fn test_xml_parse() {
        let mock_result = r#"<response xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.2" xsi:noNamespaceSchemaLocation="http://www.aviationweather.gov/static/adds/schema/metar1_2.xsd">
        <request_index>69697426</request_index>
        <data_source name="metars"/>
        <request type="retrieve"/>
        <errors/>
        <warnings/>
        <time_taken_ms>33</time_taken_ms>
        <data num_results="1">
        <METAR>
        <raw_text>KPUJ 182115Z AUTO 00000KT 2SM BR OVC004 15/15 A2998 RMK AO2</raw_text>
        <station_id>KPUJ</station_id>
        <observation_time>2023-01-18T21:15:00Z</observation_time>
        <latitude>33.92</latitude>
        <longitude>-84.93</longitude>
        <temp_c>15.0</temp_c>
        <dewpoint_c>15.0</dewpoint_c>
        <wind_dir_degrees>0</wind_dir_degrees>
        <wind_speed_kt>0</wind_speed_kt>
        <visibility_statute_mi>2.0</visibility_statute_mi>
        <altim_in_hg>29.97933</altim_in_hg>
        <quality_control_flags>
        <auto>TRUE</auto>
        <auto_station>TRUE</auto_station>
        </quality_control_flags>
        <wx_string>BR</wx_string>
        <sky_condition sky_cover="OVC" cloud_base_ft_agl="400"/>
        <flight_category>LIFR</flight_category>
        <metar_type>SPECI</metar_type>
        <elevation_m>385.0</elevation_m>
        </METAR>
        </data>
        </response>"#;

    
        let _mock = mock("GET", "/adds/dataserver_current/httpparam")
                    .with_body(mock_result).create();

        let url = &mockito::server_url();
        let full_url = url.clone() + "/adds/dataserver_current/httpparam";
        println!("Using url: {}", full_url);
        let fetcher = super::AviationWeatherFetcher::with_urls(&full_url, &full_url);
        let temp = fetcher.get_current_temperature();

        assert_eq!(15.0, temp);

    }

    #[test]
    pub fn test_backup() {
        let mock_result = r#"<response xmlns:xsd="http://www.w3.org/2001/XMLSchema" xmlns:xsi="http://www.w3.org/2001/XMLSchema-instance" version="1.2" xsi:noNamespaceSchemaLocation="http://www.aviationweather.gov/static/adds/schema/metar1_2.xsd">
        <request_index>69697426</request_index>
        <data_source name="metars"/>
        <request type="retrieve"/>
        <errors/>
        <warnings/>
        <time_taken_ms>33</time_taken_ms>
        <data num_results="1">
        <METAR>
        <raw_text>KPUJ 182115Z AUTO 00000KT 2SM BR OVC004 15/15 A2998 RMK AO2</raw_text>
        <station_id>KPUJ</station_id>
        <observation_time>2023-01-18T21:15:00Z</observation_time>
        <latitude>33.92</latitude>
        <longitude>-84.93</longitude>
        <temp_c>15.0</temp_c>
        <dewpoint_c>15.0</dewpoint_c>
        <wind_dir_degrees>0</wind_dir_degrees>
        <wind_speed_kt>0</wind_speed_kt>
        <visibility_statute_mi>2.0</visibility_statute_mi>
        <altim_in_hg>29.97933</altim_in_hg>
        <quality_control_flags>
        <auto>TRUE</auto>
        <auto_station>TRUE</auto_station>
        </quality_control_flags>
        <wx_string>BR</wx_string>
        <sky_condition sky_cover="OVC" cloud_base_ft_agl="400"/>
        <flight_category>LIFR</flight_category>
        <metar_type>SPECI</metar_type>
        <elevation_m>385.0</elevation_m>
        </METAR>
        </data>
        </response>"#;

    
        let _mock = mock("GET", "/adds/dataserver_current/httpparam")
                    .with_body(mock_result).create();

        let url = &mockito::server_url();
        let full_url = url.clone() + "/adds/dataserver_current/httpparam";
        println!("Using url: {}", full_url);
        let fetcher = super::AviationWeatherFetcher::with_urls(&(full_url.clone() + "2"), &full_url);
        let temp = fetcher.get_current_temperature();

        assert_eq!(15.0, temp);

    }

    
}

